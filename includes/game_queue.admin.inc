<?php

/**
 *  @file
 *  The administration page for the Game Queue module.
 */

/**
 *  Callback for admin/settings/game_queue.
 */
function game_queue_settings_form() {
  $types = variable_get('game_character_types', array());
  $test_types = array_unique($types);
  if (empty($types) || (sizeof($test_types) == 1 && !$test_types[0])) {
    drupal_set_message(t('You need to set at least one !node_type to be a !game_character type.', array('!node_type' => l(t('node type'), 'admin/content/types'), '!game_character' => l(t('game character'), 'admin/settings/game_character'))), 'error');
  }
  $form = array();

  return system_settings_form($form);
}

function game_queue_game_clock_form_alter() {
}
