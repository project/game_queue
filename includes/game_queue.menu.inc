<?php

/**
 *  @file
 *  Menu definition for the Game Queue module.
 */

/**
 *  Helper function for game_queue_menu().
 */
function _game_queue_menu() {
  $items = array(
    'admin/settings/game_queue' => array(
      'title' => 'Game queue',
      'description' => 'Administer the game queue.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('game_queue_settings_form'),
      'access arguments' => array('administer game queue'),
      'file' => 'includes/game_queue.admin.inc',
    ),
  );
  return $items;
}
