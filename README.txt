
Game Queue

Initial development by Aaron Winborn.

Dependent on Game Clock and Game Character.

A Game Queue will act on clock ticks, allowing game characters to add actions
to a queue, which will be processed in turn.

Actions take 1 or more Action Points (AP). A character is allowed 1 or more AP
to be processed in a single tick.

Before processing, each action is also assigned an Initiative weight.

Modules wishing to make use of this should implement
hook_game_queue_action($action). See documentation within module for more info.

@TODO:
Ensure all characters have a chance to act before enacting later actions within
the same turn.
Add dice options to settings for Available AP and Initiative.
Add a hook for action links.
Hook into forthcoming game_messages.
Remove requirement of game_character?
